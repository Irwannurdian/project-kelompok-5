package org.acme.models;

import javax.persistence.Entity;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

@Entity
@Table(name = "products")
public class Product extends PanacheEntityBase {
    /* SEQUENS */
    @Id
    @SequenceGenerator(name = "productSequence", sequenceName = "product_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "productSequence")
    public Long id;
    @NotBlank(message = "cantumkan nama produk")
    @Column(name = "name_product")
    public String nameProduct;

    @NotBlank(message = "tentuan merk produk")
    public String merk;
    @NotNull(message = "tentukan harga produk")
    public Integer price;

    @OneToOne(mappedBy = "product", cascade = CascadeType.ALL)
    public Detail detail;

    // method agar id tidak muncul otomatis
    @JsonGetter
    public Long getId() {
        return id;
    }

    @JsonIgnore
    public void setId(Long id) {
        this.id = id;
    }
}
