package org.acme;

import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.acme.models.*;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.quarkus.panache.common.Sort;

@Path("toko")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class TokoResource extends PanacheEntityBase {
   
    @GET
    public List<Toko> getToko() {
        return Toko.listAll(Sort.ascending("id"));
    }

    @POST
    @Transactional
    @Path("insertToko")
    public ResponseBuilder insertToko(@Valid Toko toko) {
        toko.persist();
        return Response.ok(toko, "toko berhasil ditambahkan");

    }

    @PUT
    @Transactional
    @Path("updateToko/{id}")
    public ResponseBuilder updateToko(@PathParam("id") Long id, @Valid Toko newToko) {
        Toko oldToko = Toko.findById(id);
        if (oldToko == null) {
            return Response.ok(oldToko, "id " + id + " tidak ditemukan!");
        }
        oldToko.nameToko = newToko.nameToko;
        oldToko.alamat = newToko.alamat;
        oldToko.nomorTel = newToko.nomorTel;
        return Response.ok(oldToko, "toko dengan id " + id + " berhasil diupdate");
    }

    @DELETE
    @Path("delete/{id}")
    @Transactional
    public ResponseBuilder deleteTokoById(@PathParam("id") Long Id) {
        boolean toko = Toko.deleteById(Id);
        if (toko == false) {
            return Response.ok(toko, "toko dengan id " + Id + " tidak ditemukan!");
        }
        return Response.ok(toko, "toko dengan id " + Id + " dihapus");
    }

   
    @DELETE
    @Path("deleteAll")
    @Transactional
    public ResponseBuilder deleteAllToko() {
        Long toko = Toko.deleteAll();
        if (toko == 0) {
            return Response.ok(toko, "tidak ada toko!");
        }
        return Response.ok(toko, "semua toko terhapus");
    }

}