# Detail Project Tugas Akhir

## **Web Service Marketplace**

Project ini merupakan sebuah **Web Service Marketplace** yang dibuat dengan bahasa pemograman **Java** yang dibantu dengan framework **Quarkus**. 

>Project ini dibuat dengan maksud untuk mengorganisir sistem Marketplace seperti Nama Toko, Produk yang tersedia dan Detail.

Project web service ini berfokus pada beberapa point dibawah ini :
1. Dependency yang digunakan.
Pada project web service marketplace ini menggunakan beberapa dependency, yaitu:
    - dependency quarkus-hibernate-orm-panache, 
    - quarkus-jdbc-postgresql, 
    - quarkus-resteasy-reactive-jackson, 
    - quarkus-smallrye-openapi dan 
    - quarkus-hibernate-validator.

2. Informasi entitas yang ada dan aturan-aturan entitas tersebut,
Pada project ini terdapat 3 entitas yaitu :
    - **Toko** 
    pada entitas bernama Toko terdapat 4 atribut yaitu *name*, *email*, *total produk* dan *location*. 
    - **Product**
    pada entitas bernama Product terdapat 3 atribut yaitu *name*, *merk*, dan *price*.
    - **Detail**
    pada entitas bernama Detail terdapat 4 atribut yaitu *garansi*, *masa garansi*, *stok*, *desciption* dan *penilaian*.

3. Hubungan entitas yang dibuat
     Hubungan entitas yang dibuat pada entitas bernama Toko agar mendapat hubungan dengan entitas bernama product menggunakan Hibernate Entity Relationship **One to Many** (satu entitas yang akan memiliki banyak entitas lainnya), dan hubungan entitas yang dibuat pada entitas bernama Product menggunakan Hibernate Entity Relationship **One to One** (satu entitas yang terdapat hubungan persis hanya ke satu entitas yang bernama Detail).

4. Format response yang dikirimkan di setiap endpoint.
pada project ini format response yang dikirim dari setiap endpoint menggunakan format response dengan method **ok ()**, dimana jika ada yang mengakses endpoint akan mengembalikan ResponseBuilder yang diprainisialisasi dengan kode *status 200*, "OK."

![Pemodelan Data Project Web Service Marketplace](https://miro.medium.com/max/1400/1*U9jfu3dEU8JCeE2MA_6mmA.webp)