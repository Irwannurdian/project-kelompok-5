package org.acme;

//import class yang dibutuhkan
import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import org.acme.models.Detail;
import org.acme.models.Product;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.quarkus.panache.common.Sort;

//membuat endpoint
@Path("details")
// memformat json
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DetailResource extends PanacheEntityBase {
    // mendapatkan seluruh detail
    @GET
    public ResponseBuilder getDetail() {
        List<Detail> detail = Detail.listAll(Sort.ascending("id"));
        return Response.ok(detail, "succes");
    }

    // mendapatkan detail dengan atribut tertentu
    @GET
    @Path("{id}")
    public ResponseBuilder getDetailByid(@PathParam("id") Long id) {
        Detail detail = Detail.findById(id);
        if (detail == null) {
            return Response.ok(detail, "detail dengan id " + id + " tidak tersedia");
        }
        return Response.ok(detail, "detail id " + id + " tersedia");
    }

    // menambahkan data detail
    @POST
    @Path("{id}")
    @Transactional
    public ResponseBuilder InsertDetail(@PathParam("id") Long id, @Valid Detail details) {
        Product product = Product.findById(id);
        if (product == null) {
            return Response.ok(product, "id " + id + " tidak ditemukan");
        }
        details.product = product;
        details.persist();
        Detail.listAll();
        return Response.ok(details, "insert detail ke produk id " + id + " berhasil");
    }

    // mengupdate data detail
    @PUT
    @Path("updateDetails/{id}")
    @Transactional
    public ResponseBuilder updateDetail(@PathParam("id") Long id, @Valid Detail newDetail) {
        Detail oldDetail = Detail.findById(id);
        if (oldDetail == null) {
            return Response.ok(oldDetail, "id " + id + " tidak ditemukan!");
        }
        oldDetail.garansi = newDetail.garansi;
        oldDetail.masaGaransi = newDetail.masaGaransi;
        oldDetail.stok = newDetail.stok;
        oldDetail.deskripsi = newDetail.deskripsi;
        oldDetail.penilaian = newDetail.penilaian;
        return Response.ok(oldDetail, "update detail dengan id " + id + " berhasil");
    }
    // menghapus detail dengan id tertentu

    @DELETE
    @Path("delete/{id}")
    @Transactional
    public ResponseBuilder deleteDetailById(@PathParam("id") Long Id) {
        boolean detail = Detail.deleteById(Id);
        if (detail == false) {
            return Response.ok(detail, "detail dengan id " + Id + " tidak ditemukan!");
        }
        return Response.ok(detail, "data dengan id " + Id + " dihapus");
    }

    // menghapus keseluruhan data
    @DELETE
    @Path("deleteAll")
    @Transactional
    public ResponseBuilder deleteAllDetail() {
        Long detail = Detail.deleteAll();
        if (detail == 0) {
            return Response.ok(detail, "tidak ada product!");
        }
        return Response.ok(detail, "semua detail dihapus");
    }

}
